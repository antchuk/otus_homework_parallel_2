﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tempapp
{
    public class FileReader
    {
        private ConcurrentBag<int> _whiteSpaceNumber = new ConcurrentBag<int>();
        public FileReader() 
        {

        }
        /// <summary>
        /// не используется, пророста производительности не получено (возможно дело в тетсовых данных)
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private Task CountSpaceInLine(string line)
        {
            int ans = 0;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == ' ')
                {
                    ans++;
                }
            }
            _whiteSpaceNumber.Add(ans);
            return Task.CompletedTask;
        }
        /// <summary>
        /// расчет кол-ва пробелов в файле по адресу path
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private async Task CalcSpaceInfile(string file)
        {
            using StreamReader sr = new(file);
            string? line;
            List<Task> _tasks = new List<Task>();
            while ((line = await sr.ReadLineAsync())!= null)
            {
                //считаем пробелы медлено  TODO попробвать с IndexOf или с рекурсией+Split                
                _tasks.Add(CountSpaceInLine(line));                
            }
            await Task.WhenAll(_tasks.ToArray());    
        }
        /// <summary>
        /// расчет количества пробелов в файлах в диреткории path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public Task<int> CalcWhiteSpaceAsync(string path)
        {                                   
            if (Directory.Exists(path)) 
            {                
                _whiteSpaceNumber.Clear();
                string[] _files = Directory.GetFiles(path);                
                List<Task> tasks = new();
                foreach (string _file in _files) 
                {
                    FileInfo inf = new FileInfo(_file);
                    Console.WriteLine($"file {inf.FullName}");
                    tasks.Add(CalcSpaceInfile(_file));
                }
                Task.WaitAll(tasks.ToArray());
                return Task.FromResult(_whiteSpaceNumber.Sum());
            }
            else
            {
                return Task.FromResult(-1);
            }
        }
    }
}
