﻿using System.Diagnostics;

namespace tempapp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = Stopwatch.StartNew();
            sw.Start();
            FileReader reader = new();
            int sum = reader.CalcWhiteSpaceAsync("F:\\data for otus_parallel_2").Result;
            Console.WriteLine($"WhiteSpace number is {sum}");
            sw.Stop();
            Console.WriteLine($"TIME ELAPSED = {sw.ElapsedMilliseconds}\npress any key to exit");
            Console.ReadKey();
        }
    }
}